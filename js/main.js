import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase,onValue,ref,get,set,child,update,remove }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes,getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyBsRlc3fgpaG7chHarZFB7q1531f9nQjh0",
    authDomain: "proyecto-2020030292.firebaseapp.com",
    databaseURL: "https://proyecto-2020030292-default-rtdb.firebaseio.com/",
    projectId: "proyecto-2020030292",
    storageBucket: "proyecto-2020030292.appspot.com",
    messagingSenderId: "883576609596",
    appId: "1:883576609596:web:397dd9eff1f978ab415c39",
    measurementId: "G-WTGZC5KK4R"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnIniciar = document.getElementById('iniciar');
var btnRegistro = document.getElementById('registrar');

var login = document.getElementById('login');
var formulario = document.getElementById('formulario');
var Contraseña;
var usuario = document.getElementById("usuario").value;
var contraseña = document.getElementById("contraseña").value;
var verificador = document.getElementById("verificador").value;
var Token;
function inicioSesion() {


    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
    if (usuario == "ADMIN" && contraseña == "ADMIN") {
        alert("BIENVENIDO ADMINISTRADOR.");
        window.location.href = "registroP.html";
    }
    else {
        alert("USUARIO O CONTRASEÑA ERRONEA");
    }
}


function buscarDatos() {

    leerInputs();
    const dbref = ref(db);
    get(child(dbref, 'usuarios/' + usuario)).then((snapshot) => {
        if (snapshot.exists()) {

            Contraseña = snapshot.val().Contraseña;
            Token = snapshot.val().Token;
            comprobar();

        }
        else {
            alert("Usuario O Contraseña Incorrectos");
        }
    }).catch((error) => {
        alert("ERROR" + " " + error);
    });

}

function verificar() {
    leerInputs();
    if (usuario == "" || contraseña == "") {
        alert("INSERTE ALGUN VALOR");
    } else {
        buscarDatos();
    }
}
function comprobar() {
    if (contraseña == Contraseña) {
        alert("Bienvenido!")
        llenarInput();
        invisible();
    }
    else {
        alert("Usuario O Contraseña Incorrectos")
    }
}
function invisible() {
    login.style.display = "none";
    formulario.style.display = "block";

}
function llenarInput() {
    document.getElementById('verificador').value = Token;
}
function leerInputs() {
    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
}
btnIniciar.addEventListener('click', verificar);