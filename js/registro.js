var IdUsuario;
var Nombre;
var Contraseña;
var Token;

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase,onValue,ref,get,set,child,update,remove }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes,getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyBsRlc3fgpaG7chHarZFB7q1531f9nQjh0",
    authDomain: "proyecto-2020030292.firebaseapp.com",
    databaseURL: "https://proyecto-2020030292-default-rtdb.firebaseio.com/",
    projectId: "proyecto-2020030292",
    storageBucket: "proyecto-2020030292.appspot.com",
    messagingSenderId: "883576609596",
    appId: "1:883576609596:web:397dd9eff1f978ab415c39",
    measurementId: "G-WTGZC5KK4R"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


function insertarDatos() {
    leerInputs();

    set(ref(db, 'usuarios/' + Nombre), {
        Contraseña: Contraseña, Token: Token
    }).then((resp) => {
        alert("Se Realizo el Registro");
    }).catch((error) => {
        alert("ERROR" + " " + error);
    })
}



function leerInputs() {

    Nombre = document.getElementById('nombre').value;
    Contraseña = document.getElementById('contraseña').value;
    Token = document.getElementById('token').value;
}

var btnRegistrar = document.getElementById('registrar');
btnRegistrar.addEventListener('click', insertarDatos);